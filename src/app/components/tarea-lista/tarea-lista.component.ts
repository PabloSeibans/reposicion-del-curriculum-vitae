import { Component, OnInit } from '@angular/core';
import { TareaService } from "../../services/tarea.service";
import { Tarea } from '../../models/task';

@Component({
  selector: 'app-tarea-lista',
  templateUrl: './tarea-lista.component.html',
  styleUrls: ['./tarea-lista.component.css']
})
export class TareaListaComponent implements OnInit {
  tasks: Tarea[];

  constructor(public tareaServicio: TareaService) { }

  ngOnInit(): void {
    this.tasks = this.tareaServicio.getTasks();
  }

  addTask(task: Tarea) {
    console.log(task);
    this.tareaServicio.addTask(task);
  }

}